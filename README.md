# CatalystMoon Base Code

New era, new branding.

Structure: 

**commands/Info** - This folder contains commands

**event** - This folder contains files related to discord.js events. (Like "ready", "interactionCreate")

**handler**  - This folder contains files that read the commands folders contents.

**index.js** - This is the main file to run the bot.

# Usage

1) Clone this repo: ```git clone https://git.codingvm.codes/LauraOrchid/CatalystMoon.git```

2) ```cd CatalystMoon```

3) Use ```npm init ```

4) Install dependencies: ```npm install discord.js dotenv glob format-duration```

5) ```nano .env``` and put this:

```
TOKEN="your bot token here"
```

4) ```nano ./handler/index.js``` and change "GUIDIDHERE" to your Discord Server's Guild ID

5) Go into https://discord.com/developers/applications and enable all 3 intents (presence, members, and message content) in your application.

6) Run the bot: ```node index.js```

Have fun! -Laura 💖
