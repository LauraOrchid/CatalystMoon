const { EmbedBuilder } = require("discord.js");

module.exports = {
  name: "hello",
  description: "Say hi to Tiffany!",

  run: async (client, interaction) => {
    const embed = new EmbedBuilder()
      .setColor("#0CE2FA")
      .setTitle("🌼 Hi friend!")
      .setDescription(`I'm Tiffany Lynch, 16 year old from Jakarta. Likes ponies, flowers, and tech.\nMy source code: https://git.codingvm.codes/LauraOrchid/CatalystMoon\nDonate to me: https://saweria.co/TiffanyLynch`)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    interaction.followUp({ embeds: [embed] });
  },
};