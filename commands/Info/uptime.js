const { EmbedBuilder } = require('discord.js');
const decodeTime = require('format-duration');

module.exports = {
  name: "status",
  description: "Tiffany's Health Status.",

  run: async (client, interaction) => {
    const embed = new EmbedBuilder()
      .setColor("#FA0CF2")
      .setTitle("💜 Tiffany's Story!")
      .setDescription(`Latency: ${client.ws.ping}ms\nUptime: ${decodeTime(client.uptime)}`)
      .setTimestamp()
      .setFooter({ text: `Requested by ${interaction.user.tag}`, iconURL: `${interaction.user.displayAvatarURL()}` });
    interaction.followUp({ embeds: [embed] });
  },
};